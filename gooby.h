#ifndef GOOBY_H
#define GOOBY_H

#include <stdint.h>
#include <stdbool.h>

typedef struct gooby_color {
  uint8_t r, g, b;
} gooby_color_t;

typedef struct gooby_rect {
  int w, h, x, y;
} gooby_rect_t;

typedef enum gooby_error {
  GOOBY_SUCCESS,
  GOOBY_ERROR_INIT,
  GOOBY_ERROR_CLOSE
} gooby_error_t;

typedef struct gooby_vector2 {
  int x;
  int y;
} gooby_vector2_t;

gooby_error_t gooby_init(const char *title, int width, int height);
gooby_error_t gooby_close();

gooby_color_t gooby_create_color(uint8_t r, uint8_t g, uint8_t b);
void gooby_set_clear_color(gooby_color_t color);
bool gooby_window_should_close();

void gooby_begin_drawing();
void gooby_end_drawing();

gooby_rect_t gooby_create_rect(int w, int h, int x, int y);
void gooby_draw_rect(gooby_rect_t rect, gooby_color_t color, bool fill);

gooby_vector2_t gooby_window_size();

#endif // GOOBY_H
