#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_video.h>
#include <stdbool.h>
#include "gooby.h"

static SDL_Window *window;
static SDL_Renderer *renderer;
static SDL_Event event;
static gooby_color_t clear_color = {
  .r = 135, 
  .g = 206, 
  .b = 235
};

gooby_error_t gooby_init(const char *title, int width, int height) {
  if (SDL_CreateWindowAndRenderer((int)width, (int)height, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE , &window, &renderer) != 0) {
    return GOOBY_ERROR_INIT;
  }
  SDL_SetWindowTitle(window, title);
  
  return GOOBY_SUCCESS;
}

gooby_error_t gooby_close() {
  SDL_DestroyWindow(window);
  SDL_DestroyRenderer(renderer);
  return GOOBY_SUCCESS;
}

gooby_color_t gooby_create_color(uint8_t r, uint8_t g, uint8_t b) {
  gooby_color_t color = {
    .r = r,
    .g = g,
    .b = b
  };
  return color;
}

bool gooby_window_should_close() {
  while (SDL_PollEvent(&event)) {
    switch (event.type) {
    case SDL_QUIT: return true;
    default: return false;
    }
  }
  
  return false;
}

void gooby_begin_drawing() {
  SDL_SetRenderDrawColor(renderer, clear_color.r, clear_color.g, clear_color.b, 255);
  SDL_RenderClear(renderer);
}

void gooby_end_drawing() {
  SDL_RenderPresent(renderer);
}

void gooby_set_clear_color(gooby_color_t color) {
  clear_color = color;
}

gooby_rect_t gooby_create_rect(int w, int h, int x, int y) {
  gooby_rect_t rect = {
    .w = w,
    .h = h,
    .x = x,
    .y = y
  };
  return rect;
}

void gooby_draw_rect(gooby_rect_t rect, gooby_color_t color, bool fill) {
  SDL_Rect sdl_rect = {
    .h = rect.h,
    .w = rect.w,
    .x = rect.x,
    .y = rect.y
  };
  SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, 255);
  
  if (fill) {
    SDL_RenderFillRect(renderer, &sdl_rect);
    return;
  }
  SDL_RenderDrawRect(renderer, &sdl_rect);
}

gooby_vector2_t gooby_window_size() {
  int x, y;
  SDL_GetWindowSize(window, &x, &y);
  gooby_vector2_t v = {
    .x = x,
    .y = y
  };
  return v;
}
