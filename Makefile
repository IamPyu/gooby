include config.mk

LIBS = sdl2 SDL2_image SDL2_ttf SDL2_mixer
CFLAGS += -Wall -Wextra -std=c99 $(shell pkg-config --cflags $(LIBS)) -fPIC
LDFLAGS += $(shell pkg-config --libs $(LIBS))
SRC = gooby.c
OBJS = $(SRC:.c=.o)

all: config libgooby.so
config:
	@echo "PREFIX: $(PREFIX)"
	@echo "CFLAGS: $(CFLAGS)"
	@echo "LDFLAGS: $(LDFLAGS)"
	@echo "LIBS: $(LIBS)"
	@echo "SRC: $(SRC)"
	@echo "OBJS: $(OBJS)"

libgooby.so: $(OBJS)
	$(CC) -shared $(CFLAGS) $(LDFLAGS) $(OBJS) -o libgooby.so

%.o: %.c
	$(CC) -c $(CFLAGS) $(LDFLAGS) $< -o $@

test: all
	$(CC) -L. -lgooby test.c -o test
	LD_LIBRARY_PATH=./. ./test
