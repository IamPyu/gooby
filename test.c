#include <stdio.h>
#include "gooby.h"

int main(void) {
  gooby_init("GAME", 800, 800);

  //gooby_set_clear_color(gooby_create_color(255, 255, 255));
  
  while (!gooby_window_should_close()) {
    gooby_begin_drawing();
    

    gooby_vector2_t size = gooby_window_size();
    printf("%d;%d\n", size.x, size.y);
    gooby_draw_rect(gooby_create_rect(150, 150, size.x / 2 + 150, size.y / 2 - 150), gooby_create_color(0, 0, 255), true);
   


    gooby_end_drawing();
  }
  
  gooby_close();
  return 0;
}
